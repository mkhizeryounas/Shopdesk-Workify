<?php
require 'ereqer/load.php';
header("Content-Type: application/json");
$res = [];
if(isset($_POST['url']) && isset($_POST['type'])) {
    $url = $_POST['url'];
    $method = $_POST['type'];
    $headers = $_POST['headers'];
    $data = $_POST['data'];    
    $res = send_data($method, $url, $data, $headers);
}
else {
    $res = [
        'status' => false,
        'message' => "params missing"
    ];
}
echo json_encode($res);