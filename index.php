<?php
	$url = "http://api.shopdesk.co/users/login";
	$method = "";
	$headers = [
		'Authorization'=> 'API-KEY-HERE'
	];
	$data = "";
	if(isset($_GET['payload'])) {
		$payload = json_decode($_GET['payload'], true);
		// echo "<pre>";
		// print_r($payload);
		// echo "</pre>";
		$url = $payload['url'];
		$method = $payload['method'];
		$headers = $payload['headers'];
		$data = json_encode($payload['data'],JSON_PRETTY_PRINT);
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>SD | Workify</title>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
		<style type="text/css">
		pre {outline: 1px solid #ccc; padding: 5px; margin: 5px; }
		.string { color: green; }
		.number { color: darkorange; }
		.boolean { color: blue; }
		.null { color: magenta}
		textarea { 
			resize: vertical;
		}
		#editor { 
			height:230px;
			border: 2px solid #eee;
			border-radius: 2px;
		}
	</style>
	<style>
		#loader {
			display: none;
		}
		.loader {
		  border: 6px solid #f3f3f3;
		  border-radius: 50%;
		  border-top: 6px solid #3498db;
		  width: 50px;
		  height: 50px;
		  -webkit-animation: spin 0.8s linear infinite; /* Safari */
		  animation: spin 0.8s linear infinite;
		}
		
		/* Safari */
		@-webkit-keyframes spin {
		  0% { -webkit-transform: rotate(0deg); }
		  100% { -webkit-transform: rotate(360deg); }
		}
		
		@keyframes spin {
		  0% { transform: rotate(0deg); }
		  100% { transform: rotate(360deg); }
		}
		</style>
	<script type="text/javascript">
		var editor;
		$(function() {
			$('#new-param').click(function() {
				// console.log('click')
				$('#params').append('<tr>					<td class="form-group col-md-1">						<button class="del btn btn-default	"><i class="fa fa-trash"></i></button>					</td>					<td class="form-group col-md-5">					<input type="text" class="form-control params-name" placeholder="name">					</td>					<td class="form-group col-md-6">						<textarea type="text" class="form-control params-value" placeholder="value" rows="1"></textarea>					</td>				<tr>');
			})
			$(document).on('click', '.del', function(){
				$(this).parent('td').parent('tr').remove();
			})
			$(document).on('click', '#send', function() {
				var url = $('#apiUrl').val();
				var method = "";
				if($('#get').prop('checked'))
					method = "GET";
				else 
					method = "POST";
				var headers = {};
				$('#headers').find('tr').each(function() {
					var hname = $(this).find('.head-name').val();
					var hval = $(this).find('.head-value').val();
					if(typeof hname !== "undefined" && typeof hval !== "undefined" && hname !== "" && hval !== "") {
						headers[hname]=hval;
					}
				})
				var data = {}
				if(editor.getValue())
					data = JSON.parse(editor.getValue());
				// var data = {};
				// $('#params').find('tr').each(function() {
				// 	var pname = $(this).find('.params-name').val();
				// 	var pval = $(this).find('.params-value').val();
				// 	if(typeof pname !== "undefined" && typeof pval !== "undefined" && pname !== "" && pval !== "") {
				// 		data[pname]=pval;
				// 	}
				// })
				if(typeof url !== "undefined" && url !== "") {
					$('#loader').fadeIn();
					data['sd-source'] = "sd-workify-dev";
					$.ajax({
						// url: "sd-workify-fetcher.php",
						url: url,
						method: method,
						headers: headers,
						data : data,
						success: function(res) {
							console.log(res);
							$('#loader').fadeOut();
							$('#res').html(JSON.stringify(res, undefined, 2));
						},
						error: e => {
							$('#loader').fadeOut();
							$('#res').html("Error occoured");
						}
					})
				}
			})

		})
		function gotoPayload() {
			window.location = payloadShare();
		}
		function payloadShare() {
			let loc=window.location;
			let res = loc.Location
			return(loc.origin+loc.pathname+"?payload="+saveState());
		}
		function saveState() {
				var url = $('#apiUrl').val();
				var method = "";
				if($('#get').prop('checked'))
					method = "GET";
				else 
					method = "POST";
				var headers = {};
				$('#headers').find('tr').each(function() {
					var hname = $(this).find('.head-name').val();
					var hval = $(this).find('.head-value').val();
					if(typeof hname !== "undefined" && typeof hval !== "undefined" && hname !== "" && hval !== "") {
						headers[hname]=hval;
					}
				})
				var data;
				if(editor.getValue())
					data = JSON.parse(editor.getValue());
				// var data = {};
				// $('#params').find('tr').each(function() {
				// 	var pname = $(this).find('.params-name').val();
				// 	var pval = $(this).find('.params-value').val();
				// 	if(typeof pname !== "undefined" && typeof pval !== "undefined" && pname !== "" && pval !== "") {
				// 		data[pname]=pval;
				// 	}
				// })
				if(typeof url !== "undefined" && url !== "") {
					return JSON.stringify({
						url: url,
						method: method,
						headers: headers,
						data: data
					})
				}
				else 
					return false;	
		}
		function jsonToFormData(json) {
            const formData = new FormData();
            Object.keys(object).forEach(key => formData.append(key, object[key]));
            return formData;
		}
	</script>
	<script src="https://ace.c9.io/build/src/ace.js" type="text/javascript"></script>
	<script>
		$(function() {
			editor = ace.edit("editor");
			// editor.setTheme("ace/theme/monokai");
			editor.session.setMode("ace/mode/javascript");
		})
	</script>
</head>
<body>
	
	<div class="col-sm-12">
		<h1 class="col-sm-6"><tt>SD|<b>Workify</b></tt> </h1>
		<div class="col-sm-6 form-group">
			<br>
			<button class="btn btn-default btn-sm pull-right" onclick="gotoPayload()">Shareable Payload</button>
		</div>
		<div class="clearfix"></div>
		<hr>
	</div>
	<div class="col-md-6" style="border-right: 2px dotted #eee;">
		<div class="col-md-7 form-group">
			<label>API Endpoint</label>
			<input type="text" id="apiUrl" class="form-control" placeholder="http://api.shopdesk.co/users/login" value="<?php echo $url ?>">
		</div>
		<div class="col-md-3 form-group">
			<label>Call Method</label><br>
			<label><input type="radio" id="get" name="type" <?php echo ($method=='GET')?'checked':'' ?> > GET</label>
			<label><input type="radio" id="post" name="type"  <?php echo ($method=='POST' || $method=='')?'checked':'' ?>> POST</label>
		</div>
		
		<div class="col-md-2 form-group">
			<label>&nbsp;</label>
			<button id="send" class="btn btn-primary form-control">Send</button>
		</div>
		<div class="clearfix"></div>

		<hr>
		<div id="" class="col-md-12 form-group">
			<label>Headers </label>
			<div class="clearfix"></div>
			<table class="table table-hover" id="headers">
				<thead>
					<tr>
						<th>Name</th>
						<th>Value</th>
					</tr>
				</thead>
				<tr>
				<?php 
					foreach($headers as $k => $v) {
						echo '<td class="form-group col-md-6">
								<input type="text" class="form-control head-name" placeholder="name" value="'.$k.'">
							</td>
							<td class="form-group col-md-6">
								<input type="text" class="form-control head-value" placeholder="value"  value="'.$v.'">
							</td>';	
					}
				?>
			</tr>
			<tr>
				<td class="form-group col-md-6">
					<input type="text" class="form-control head-name" placeholder="name">
				</td>
				<td class="form-group col-md-6">
					<input type="text" class="form-control head-value" placeholder="value">
				</td>
			</tr>
			<tr>
				<td class="form-group col-md-6">
					<input type="text" class="form-control head-name" placeholder="name">
				</td>
				<td class="form-group col-md-6">
					<input type="text" class="form-control head-value" placeholder="value">
				</td>
			</tr>
			</table>
		</div>

		<div class="clearfix"></div>
		<hr>
		<div class="col-md-12 form-group">
			<label>Params <!--span class="">&nbsp;&nbsp; <a href="javascript:void(0);" id="new-param" class="pull-right"><i class="fa fa-plus"></i> New Param</a></span--></label>
			<div class="clearfix"></div>
			<!-- <table id="params" class="table table-hover">
				<thead>
					<tr>
						<th></th>
						<th>Name</th>
						<th>Value</th>
					</tr>
				</thead>
				<?php 
					foreach($data as $k => $v) {
						echo '
							<tr>
								<td class="form-group col-md-1">
									<button class="del btn btn-default"><i class="fa fa-trash"></i></button>
								</td>
								<td class="form-group col-md-5">
									<input type="text" class="form-control params-name" placeholder="name"   value="'.$k.'">
								</td>
								<td class="form-group col-md-6">
									<textarea type="text" class="form-control params-value" placeholder="value" rows="1">'.$v.'</textarea>
								</td>
							<tr>

							';	
					}
				?><tr>
					<td class="form-group col-md-1">
						<button class="del btn btn-default"><i class="fa fa-trash"></i></button>
					</td>
					<td class="form-group col-md-5">
						<input type="text" class="form-control params-name" placeholder="name" >
					</td>
					<td class="form-group col-md-6">
						<textarea type="text" class="form-control params-value" placeholder="value" rows="1" ></textarea>
					</td>
				<tr>
					
			</table> -->

			
			<div id="editor"><?php echo $data?></div>
		</div>
	</div>
	<div class="col-md-6">
		<h4>Response</h4><hr>
		<center id="loader"><div class="loader"></div><br></center>		
		<pre id="res"  style="max-height: 90vh; overflow:auto;">{{ response }}</pre>
	</div>
	<div class="clearfix"></div>
	<hr>
</body>
</html>
