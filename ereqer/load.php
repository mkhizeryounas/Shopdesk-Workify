<?php
require 'EasyRequest.php';
function send_data($method, $url, $data=[], $headers=[], $auth=[]) {
	$options = ['headers' => $headers];
	if($method == 'POST')
		$options['form_params'] = $data;
	else
		$options['query'] = $data;
	// var_dump($options);
    $client = EasyRequest::create($url, $method, $options)->send();
	return json_decode($client->getResponse()['body'], true);
}
